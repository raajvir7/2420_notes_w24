# Learning outcomes and topics

- Introductions
- Course overview
- Tips for success
- Linux, What Why Where and Who
- Tools overview
	- terminal emulator (terminal vs shell)
	- Create a digitalocean account
	- Create an SSH key pair
	- Create a "droplet"

# Introductions

## About me

- Nathan McNinch
- I started using Linux in the late 90s, Slackware was my first distro. I installed it on an old computer I got from my uncle with my father.
- Favourite tool that I have used since I started working at BCIT
	- [NixOS](https://nixos.org/) 
	- or [Pulumi](https://www.pulumi.com/)
- Outside of class I like 
	- climbing (bouldering particularly)
	- coffee, I am a coffee nerd
	- comics Most of the series that I am into right now are [Image](https://imagecomics.com/) comics.

## About you

- What is your name
- Do you have any previous experience with a Linux OS or BSD OS? not including 1420
- What is your favourite tool, programming language, technology that you have used so far at BCIT
- Outside of class what do you like to do? #1 hobby

# Course overview

## Course notes

The course notes are distributed in a [GitLab repository](https://gitlab.com/cit2420/2420_notes_w24) as plain text, markdown documents. You can simply view the notes on GitLab, but for best results incorporate them into your own notes. I am going to use Obsidian to write the class notes.

## Office hours

Please schedule appointments at least 48 hours in advance. When you schedule an appointment let me know what you want to talk about. This way I can prepare material if needed.

If these times don't work for you, please get in touch with me and we can figure out a time that works for both of us.

- Monday 08:30-10:30 location TBD
- Tuesday 11:30-12:30 location TBD
- Friday 08:30-10:30 location TBD
## course breakdown

| Item | Percentage | Notes |
| ---- | ---- | ---- |
| Midterm | 25 | Pen and paper |
| Final | 25 | cumulative, pen and paper |
| Assignment 1 | 20 | Group research presentation |
| Assignment 2 | 20 | Shell scripting |
| Assignment 3 | 15 | Web server |
## Making recordings in class

Most of the time anything on screen in class I will share with you. So you don't need to take pictures of the screen. If for some reason you would like to make any kind of recording in class you are required to ask first. 

https://www.bcit.ca/files/pdf/policies/5201.pdf 

The rest of the course outline is available [here](https://www.bcit.ca/programs/computer-information-technology-diploma-full-time-5540dipma/#courses)

# Tips for success

## Stay on top of the flipped material

Find the time to read/watch as much (preferably all) of the content in the flipped material as you can. Take notes, and ask questions as they arise. 

## Make good use of lab time

Work collaboratively with your classmates during lab time.

## Take notes and review your notes

Take notes in class, take notes when you read/watch flipped material.

review your notes.

### More on taking notes

#### Note taking tools

Note taking tools, and PKM have been hot topics for the past few years, as such there are a lot of awesome note taking tools out there. This is just a small selection of some that I think have benefits for students.

- [Logseq](https://logseq.com/) (free)
	- Logseq includes a great [flashcard](https://docs.logseq.com/#/page/flashcards) feature, that uses spaced repetition to help you study.
	- Good youtube integration for taking time stamped notes on a youtube video.
	- Good PDF integration, for taking notes from a PDF.
- [Obsidian](https://obsidian.md/) (free)
- [Heptabase](https://heptabase.com/)
- [Tana](https://tana.inc/)

All of the above have three important features
1. daily notes
2. wiki links or back links for making networked notes
3. Are great tools for creating atomic notes
### A simple note taking strategy

#### Class notes

Don't try to write down everything I say. Treat class notes like a todo list. 
Write down important concepts and questions that you have. You can later search for the answers on your own or ask me for more info.

Write your class notes in your daily notes.

#### Reading notes

Take notes on all of the flipped material. 
- Copy and paste material that you think is important.
- Write more questions, things you don't fully understand

You could write these in your daily notes as well. Or if you are using a single textbook you may want to create a section for that resource.

#### Atomic notes

Review your class and reading notes and paraphrase the material in them into your own "atomic" notes.

By atomic I mean break down concepts into small chunks. For example you could have a note that is just on creating variables in JavaScript.

Use wiki links to associate notes with other notes. For example JavaScript variables would be associated with JavaScript types. You might have a MOC (map of content) for types in one document with links to individual notes on each type.

Writing the notes yourself and paraphrasing will help you learn the material. 
Inter-linked atomic notes will make it easier for you to find your material.

#### flash cards

Create some flash cards on the material and try to review these cards a few times a week. Logseq has a great feature for this, but you can do this in any note taking tool, or use pen and paper.

Writing your own questions will help you learn the material far more than simply reviewing example questions provided in class.

## AI

The short version, you can use AI to help you study, you **cannot** use AI to do your assignments.

Examples of *acceptable* use:
- Ask AI to generate question, based on flipped content, to use as flash cards to help you study.
- Use AI to help you understand concepts in the flipped material. 

Examples of *unacceptable* use:
- Use AI to write a portion of your shell scripts in assignment 2. This includes using tools like Copilot. Using tools like this will hinder your learning and can result in academic integrity issues.
- Use AI as a source of information in assignment 1.

> [!note]
> There is a short video on AI in the flipped learning material section, watch the entire video. Particularly the section on learning methods and the benefits of hard work.

# So What is Linux?

>[!question] What do you think Linux is?

## What is Linux?

Linux is a family of operating systems that use the Linux kernel.

The Linux kernel was created by Linus Torvalds in 1991 when Linus was an undergrad. Linus also created Git, to work on the Linux kernel. Today the Linux kernel is maintained by thousands of people and organisations. These organisations include: Microsoft, Amazon and Google.

A Linux operating system will generally include other software like the GNU of busybox utils, an init system and any number of applications specific to that OS' intended purpose, desktop, web server...

A Linux OS is generally referred to as a distribution or "*distro*".

### The Linux kernel

![linux kernel](attachments/ArticleImage-12104-1.webp)

### Linux distros

![Linux distro timeline](./attachments/Linux_Distribution_Timeline.svg)

We are going to use [Arch Linux](https://archlinux.org/) this term.

## Where is Linux used

>[!question] Where do you think Linux is used today?
>Why do you think Linux is used for those things?

## Server vs desktop

Many Linux based OS have a Server edition and Desktop edition. In this class we are going to focus on Servers. This means that we are going to do most of our work in a text based environment, in a terminal.

At first working in the terminal may seem difficult, but as you get more familiar with working in the terminal you will start to appreciate just how powerful it is. It is an essential skill for anyone interested in development. 

My advice on getting more comfortable with it is to practice, and accept that it is different. Try to use the terminal to do things you currently do in a GUI. For example the next time you create a directory, use the terminal instead of the file explorer.

All of that said Linux as your daily driver, on the Desktop is awesome! I highly encourage you to try it out. And I am happy to discuss this and other Linux related material that is not directly related to the course.

# Terminals

Installing a terminal. Chances are you already have a terminal emulator installed on your system. 

Linux OS' will usually come with *Gnome terminal* (Gnome) or *Konsole* (KDE).  

MacOS comes with *terminal* (most mac users install one of the terminals listed below).

As of Windows 11 windows comes with the *Windows Terminal*.

Some editors include a builtin terminal, VSCode, Neovim...

A terminal emulator is a really important part of your development working kit. A software developer, DevOps engineer... will use their terminal everyday. As such spending the time to pick one and configure it to your liking shouldn't be viewed as a waste of time.

**Here are a few cross platform terminals you may want to consider:**

[Alacritty](https://alacritty.org/) (Linux, Mac and Windows)

[Wezterm](https://wezfurlong.org/wezterm/index.html) (Linux, Mac and Windows)

[Kitty](https://sw.kovidgoyal.net/kitty/) (Linux and Mac)

>[!note] terminal config examples
>here are configuration examples for the above terminals to help you get started
>https://gitlab.com/nathan-climbs/example-terminal-configs.git

## Terminal vs shell

Something new developers often struggle with is the distinction between the terminal and shell.

*The terminal* is an interface, the GUI that you interact with.

*The shell* is software that runs the commands you enter in the terminal.

For example in Windows you might open up the Windows Terminal and use PowerShell to run commands on your system

We are going to be using the **Bash** shell in Linux. In MacOS you are probably using **zsh**. And in Windows you are probably using the **PowerShell**. You can install zsh, PowerShell and many other shells on most Linux distributions.

> [!warning] Local vs Remote environments
> Throughout the term we will be running commands on both a remote Linux OS and your local machine. Make sure you pay attention to which environment you are working in! 

# Digitalocean

[Digitalocean](https://www.digitalocean.com/) is a developer focused cloud service provider.

We are going to use digitalocean "droplets" as our Linux environment for the term. These are remote Linux virtual machines running on servers owned by digitalocean. 

Before you create a digitalocean account signup for the GitHub student developer pack.

> [!note] GitHub student developer pack
> This includes a $200 credit for digitalocean, this is more than enough for the term.
> https://education.github.com/pack

## Create a digitalocean account

This part is relatively straightforward. Go to digitalocean and sign up for an account. You can do this with a credit card or a paypal account. There will be a small charge, usually 0.30$ to $5. This is just to confirm who you are. You should be refunded this amount and afterwards you can use the credit above.

## Create an ssh key pair

> The Secure Shell (SSH) protocol is a method for securely sending commands to a computer over an unsecured network. SSH uses cryptography to authenticate and encrypt connections between devices. SSH also allows for [tunneling](https://www.cloudflare.com/learning/network-layer/what-is-tunneling/), or port forwarding, which is when data [packets](https://www.cloudflare.com/learning/network-layer/what-is-a-packet/) are able to cross networks that they would not otherwise be able to cross. SSH is often used for controlling servers remotely, for managing infrastructure, and for transferring files.
> - [Cloudflare What is SSH?](https://www.cloudflare.com/learning/access-management/what-is-ssh/)

SSH uses two keys, a public key(this goes on the server) and a private key(this stays on your host machine) for authentication. It also relies on a server(this runs on the server) and a client(your host machine).

>[!example] Create a new SSH key pair
>```bash
>ssh-keygen -t ed25519 -f ~/.ssh/do-key -C "youremail@email.com"
>```

>[!example] Copy your public key from the command line
>Powershell
>```
>Get-Content C:\Users\user-name\.ssh\do-key.pub | Set-Clipboard
>```
>MacOS
>```
>pbcopy < ~/.ssh/your-key.pub
>```

>[!example] Connect to your server using ssh
>```
>ssh -i .ssh/do-key root@your-droplets-ip-address
>```

### SSH config file

If you don't like typing the above to connect to server, you could create a config file.

Inside of your .ssh directory create a new file named "config"  `~/.ssh/config`.

Using the example configuration below you would connect to your server with the command `ssh linux` . Don't forget to change the "HostName" and "User" values to match your server.

```
#github
Host github.com
  HostName github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gh-key

#DO
Host linux
  HostName you-servers-ip-address
  User username-on-server
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/do-key
```

**Reference:**
- [OpenSSH](https://www.openssh.com/)
- [How to use SSH to Connect to a Remote Server](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server)
- [What is SSH?](https://www.cloudflare.com/learning/access-management/what-is-ssh/)
## Add your SSH public key to digitalocean and create your first droplet

We are going to go through this in class

Arch Linux isn't one of the officially supported Linux distros available on DigitalOcean, so we need to add an image.

https://gitlab.archlinux.org/archlinux/arch-boxes/-/packages

You want the most recent qcow2 cloud image.

Cloud config file changes:
- Change the user name and primary group (these values should be the same)
- Change the ssh key to your ssh public key, everything after the '- '

```yml
#cloud-config
users:
  - name: bob
    primary_group: bob
    groups: wheel
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL3621LA64e7u7au23z8HST2vkn6aIwOO1MJyA0uXbsW example@email.com

packages:
  - ripgrep
  - rsync
  - vim
  - fd
  - less
  - man-db
  - bash-completion
  - tmux

runcmd:
  - echo 'PermitRootLogin no' > /etc/ssh/sshd_config.d/01-permitrootlogin.conf
  - systemctl restart sshd
```

**Reference:**

https://www.youtube.com/watch?v=ORcvSkgdA58

https://docs.digitalocean.com/products/droplets/how-to/

https://docs.digitalocean.com/products/images/custom-images/

# flipped learning material

Complete the flipped learning material before next weeks class

[Chapter 2: The Shell And Its Commands](https://learning.oreilly.com/library/view/linux-for-system/9781803247946/B18575_02.xhtml), Linux for System Administrators

[Vox AI can do your homework. Now what?](https://www.youtube.com/watch?v=bEJ0_TVXh-I) 

# Todo

- Make sure that you have a digitalocean droplet running Arch Linux that you can connect to via SSH from your host machine
- Read Chapter 2, from Linux for System Administrators
- Watch the Vox video on AI in the education